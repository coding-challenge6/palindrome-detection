# library for application functions
import string
import re
import sys


def is_palindrome(input_string):
    """This function takes in string, removes spaces, punctuation and converts to lowercase
    compares to reverse of itself to determine if a palindrome, returns boolean"""
    initial_str = re.sub(r'[^\w\s]', '', input_string.translate({ord(c): None for c in string.whitespace})).lower()
    reverse_str = re.sub(r'[^\w\s]', '', input_string.translate({ord(c): None for c in string.whitespace})[::-1]).lower()
    if initial_str == reverse_str:
        return True
    else:
        return False


def replay():
    """This function is used to continue or terminate the application, takes in  an input
    from command line and restarts app if yes and terminates app if no, other inputs terminate app"""
    replay_str = input("Would you like to test another word/phrase? [Y/N]")
    if re.match('^[Yy].*', replay_str):
        user_input()
    elif re.match('^[Nn].*', replay_str):
        print("Goodbye")
        sys.exit()
    else:
        print("Goodbye")
        sys.exit()


def user_input():
    """This function allows user to input a string into the command line and return
    a yes or no response depending on if that string is a palindrome or not, this is
    the function called in the main application"""
    text = input("enter word or phrase to see if it's a palindrome\n")

    test = is_palindrome(text)

    if test:
        print(f"{text} is a palindrome")
        replay()
    else:
        print(f"{text} is not a palindrome")
        replay()
