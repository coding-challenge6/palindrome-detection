from unittest import TestCase, mock
from lib import is_palindrome, replay, user_input
from io import StringIO
import sys


class TestPalindromeDetection(TestCase):
    """This test case runs different strings with different character combinations to test
    ability for function to parse palindromes from complex inputs"""
    def test_palindrome_no_special_characters(self):
        test_result = is_palindrome("radar")
        self.assertTrue(test_result)

    def test_palindrome_with_spaces(self):
        test_result = is_palindrome("a man a plan a canal panama")
        self.assertTrue(test_result)

    def test_palindrome_with_special_characters(self):
        test_result = is_palindrome("a man, a plan, a canal, panama!")
        self.assertTrue(test_result)

    def test_palindrome_with_special_characters_caps(self):
        test_result = is_palindrome("A man, a Plan, A Canal, Panama!")
        self.assertTrue(test_result)

    def test_not_a_palindrome(self):
        test_result = is_palindrome("palindrome")
        self.assertFalse(test_result)

    def test_not_a_palindrome_with_spaces(self):
        test_result = is_palindrome("this is not a palindrome")
        self.assertFalse(test_result)

    def test_not_a_palindrome_with_special_characters(self):
        test_result = is_palindrome("this! is not a palindrome!")
        self.assertFalse(test_result)

    def test_not_a_palindrome_with_special_characters_caps(self):
        test_result = is_palindrome("This!! is Not A Palindrome!")
        self.assertFalse(test_result)


class TestUserInput(TestCase):
    """this test case runs several command line option scenarios and tests that the application
    runs and exits with valid inputs"""
    def test_input_palindrome(self):
        with self.assertRaises(SystemExit):
            sys.stdin = StringIO('radar\nN')
            user_input()

    def test_input_not_a_palindrome(self):
        with self.assertRaises(SystemExit):
            sys.stdin = StringIO('palindrome\nN')
            user_input()

    def test_input_yes_replay(self):
        with self.assertRaises(SystemExit):
            sys.stdin = StringIO('radar\nY\na toyota\nN')
            user_input()

    def test_replay_yes_full(self):
        with self.assertRaises(SystemExit):
            sys.stdin = StringIO('yes\nradar\nno')
            replay()

    def test_input_no_full(self):
        with self.assertRaises(SystemExit):
            sys.stdin = StringIO('no')
            replay()

    def test_replay_yes_partial(self):
        with self.assertRaises(SystemExit):
            sys.stdin = StringIO('y\nradar\nno')
            replay()

    def test_input_no_partial(self):
        with self.assertRaises(SystemExit):
            sys.stdin = StringIO('n')
            replay()
