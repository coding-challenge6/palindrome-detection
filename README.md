# Palindrome Detector

Command Line application for determining if a word or phrase is a palindrome:
e.g. "A man, a plan, a canal, Panama!"

## Getting started

Installation steps:

Clone Gitlab repository:

```commandline
cd <parent_directory>
git clone https://gitlab.com/coding-challenge6/palindrome-detection.git
```

Install, create and activate  virtual environment: 

```commandline
sudo apt install python3-virtualenv
cd <project_folder>
virtualenv palindrome_env
source palindrome_env/bin/activate
```

## Testing Application

To run unit tests for this application

```commandline
py -m unittest tests.py
```

## Running Application

In project directory, enter this command:

```commandline
py palindrome.py
```

Application will prompt user to enter word or phrase:

```commandline
enter word or phrase to see if it's a palindrome
A man, a plan, a canal, panama
```

Application will return results and prompt user for another phrase

```commandline
A man, a plan, a canal, panama is a palindrome
Would you like to test another word/phrase? [Y/N]yes
```

Entering "No" will terminate application

```commandline
Goodbye
```

## Links

- [Repository](https://gitlab.com/coding-challenge6/palindrome-detection)
- [List of Palindromes](https://en.wiktionary.org/wiki/Appendix:English_palindromes)
